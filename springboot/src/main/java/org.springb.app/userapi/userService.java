package org.springb.app.userapi;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class userService {
	
	private final userRepository repo;

	 public PaginatedUserResponse readUsers(Pageable pageable) {
	        Page<User> users = repo.findAll(pageable);
	        return PaginatedUserResponse.builder()
	                .numberOfItems(users.getTotalElements()).numberOfPages(users.getTotalPages())
	                .bookList(users.getContent())
	                .build();
	    }

	    public PaginatedUserResponse readUsersWithSorting(Pageable pageable) {
//	        Pageable pageableWithSorting = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize());
	        Page<User> users = repo.findAll(pageable);
	        return PaginatedUserResponse.builder()
	                .numberOfItems(users.getTotalElements()).numberOfPages(users.getTotalPages())
	                .bookList(users.getContent())
	                .build();
	    }

	    public PaginatedUserResponse filterUsers(String firstname, Pageable pageable) {

	        //Page<User> users = repo.findAllByNameContains(firstname, pageable);
	    	Page<User> users = repo.findAll(pageable);
	        return PaginatedUserResponse.builder()
	                .numberOfItems(users.getTotalElements()).numberOfPages(users.getTotalPages())
	                .bookList(users.getContent())
	                .build();
	    }
	
}