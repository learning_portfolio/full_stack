package org.springb.app.userapi;

import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;
import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/user")
public class userAPI{
	
	@Autowired
	private userRepository repo;
	
	private final userService service;
	
	
	@PostMapping()
	public String create(@RequestBody User user) {
		
		user.setCreated(LocalDateTime.now().toString());
		user.setUpdated(LocalDateTime.now().toString());
		
		repo.save(user);
		return "creating";
	}
	
//	@GetMapping("/read")
//	public List<User> read() {
//		return repo.findAll();
//	}
	
	@PutMapping()
	public String update() {
		return "update";
	}
	
	@DeleteMapping()
	public String delete() {
		return "delete";
	}
	
	
	@GetMapping()
    public ResponseEntity readUsers (Pageable pageable) {
        return ResponseEntity.ok(service.readUsers(pageable));
    }

    @GetMapping("/sorting")
    public ResponseEntity readUsersWithSorting (Pageable pageable) {
        return ResponseEntity.ok(service.readUsersWithSorting(pageable));
    }

    @GetMapping("/filter")
    public ResponseEntity readUsersWithFilter (@RequestParam("query") String query, Pageable pageable) {
        return ResponseEntity.ok(service.filterUsers(query, pageable));
    }
	    
	
}