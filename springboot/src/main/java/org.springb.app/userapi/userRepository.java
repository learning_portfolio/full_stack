package org.springb.app.userapi;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface userRepository extends JpaRepository<User, Integer>, PagingAndSortingRepository<User,Integer>  {

	List<User> findByEmail(String email);
	
	//Page<User> findAllByNameContains(String firstname, Pageable pageable);
}
