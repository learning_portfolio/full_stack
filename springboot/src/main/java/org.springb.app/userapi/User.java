package org.springb.app.userapi;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.CreatedDate;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import lombok.Getter;
import lombok.Setter;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table
public class User{
		
		@Id
		@GeneratedValue
		private int id;
		private String firstname;
		private String lastname;
		private String email;
		@CreatedDate
		private String created;
		@LastModifiedDate
		private String updated;
	
}