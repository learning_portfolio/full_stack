package org.springb.app.userapi;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PaginatedUserResponse {
    private List<User> bookList;
    private Long numberOfItems;
    private int numberOfPages;
}
