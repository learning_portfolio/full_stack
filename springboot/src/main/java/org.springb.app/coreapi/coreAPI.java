package org.springb.app.coreapi;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class coreAPI{
	
	@GetMapping("/health")
	public String getMessage() {
		return "Welcome to Spring api..!!";
	}
	
	@GetMapping("/ping")
	public String ping() {
		return "OK";
	}
}