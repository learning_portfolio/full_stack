
import React, {useEffect} from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { FiSettings, FiBell } from 'react-icons/fi';
import { TooltipComponent } from '@syncfusion/ej2-react-popups';

import './App.css';

function App() {
    const activeMenu = true;
  return (
    <BrowserRouter>
    <div className="flex relative dark:bg-main-dark-bg">
        <div className="fixed right-4 top-4" style={{ zIndex: '1000'}}>
          <TooltipComponent content="settings" position="Bottom">
           <button>
            <FiBell />
           </button>
           &nbsp; &nbsp;
           <button>
            <FiSettings />
           </button>
          </TooltipComponent>
        </div>
        {activeMenu ?(
            <div className="w-72 fixed sidebar dark:bg-secondary-dark-bg bg-white">
                Sidebar
            </div>
        ): (
            <div>
                Sidebar 2
            </div>
        )
        }
        <div className="(activeMenu ? 'dark:bg-main-bg bg-main-bg min-h-screen md:ml-72 w-full': 'dark:bg-main-bg bg-main-bg min-h-screen  w-full')">
        </div>
    </div>
    </BrowserRouter>
  );
}

export default App;
