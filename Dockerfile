FROM nginx:latest

COPY default.conf /etc/nginx/conf.d/default.conf

# setting work directory
WORKDIR /usr/share/nginx/html
COPY ./html/ .

